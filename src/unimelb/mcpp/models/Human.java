package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;

public class Human extends Creature {

  @Override
  boolean isOccupied() {
    return true;
  }

  @Override
  public void display() {
    System.out.print("H");
  }

  @Override
  public int getUICreature() {
    return UICreature.Human.intValue();
  }

  @Override
  public int getTypeAsInt() {
    return 1;
  }
}
