package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;


public class Dead extends Creature {
	 @Override
	  boolean isOccupied() {
	    return true;
	  }

	  @Override
	  public void display() {
	    System.out.print("D");
	  }

	  @Override
	  public int getUICreature() {
	    return UICreature.Dead.intValue();
	  }

	  @Override
	  public int getTypeAsInt() {
	    return 4;
	  }

}
