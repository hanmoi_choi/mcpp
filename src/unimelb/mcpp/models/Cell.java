package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;

public class Cell {
  private Creature creature;
  private int atX;
  private int atY;

  public Cell(int atX, int atY) {
    this.atX = atX;
    this.atY = atY;
  }

  public Creature getCreature() {
    return creature;
  }

  public void setCreature(Creature creature) {
    this.creature = creature;
  }

  public boolean isOccupied(){
    return creature.isOccupied();
  }

  public void display() {
    creature.display();
  }

  public int getUIValue() {
    return creature.getUICreature();
  }
}
