package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;

public class NullObj extends Creature {

  @Override
  boolean isOccupied() {
    return false;
  }

  @Override
  public void display() {
    System.out.print(" ");
  }

  @Override
  public int getUICreature() {
    return UICreature.Empty.intValue();
  }

  @Override
  public int getTypeAsInt() {
    return 0;
  }
}
