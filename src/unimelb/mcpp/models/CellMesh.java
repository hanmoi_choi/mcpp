package unimelb.mcpp.models;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mpi.MPI;

public class CellMesh {

  /*
   * Tag 0 is for updating UI
   * Tag 1 is to calculate total number of Zombies
   * Tag 2 is to calculate total number of Humans
   * Tag 3 is to control the number of zombies
   * Tag 4 is to control the number of humans
   * Tag 5 is to copy ghost cell to North Mesh
   * Tag 6 is to copy ghost cell to East  Mesh
   * Tag 7 is to copy ghost cell to South Mesh
   * Tag 8 is to copy ghost cell to West Mesh
   */

  private static final int NUM_OF_RECV_THREADS = 4;
  private final int rank;
  private int atX;
  private int atY;
  private int size;
  private Cell[][] cells;
  private int attack = 0;
  private int statechange = 0;

  // For copying Ghost Cells
  private int southRankToSend;
  private int northRankToSend;
  private int eastRankToSend;
  private int westRankToSend;

  private int northRankToRecv;
  private int eastRankToRecv;
  private int southRankToRecv;
  private int westRankToRecv;

  private Random random;
  private ExecutorService executor;


  public CellMesh(final int rowInMesh, final int colInMesh, int size, int rank, int meshRowSize,
                  int meshColSize) {
    this.atX = rowInMesh;
    this.atY = colInMesh;
    this.size = size;
    this.rank = rank;
    random = new Random();
    random.setSeed(System.currentTimeMillis());
    cells = new Cell[size][size];
    executor = Executors.newFixedThreadPool(NUM_OF_RECV_THREADS);

    calculateRankForCopyingGhostCells(rank, meshRowSize, meshColSize);

    for (int row = 0; row < size; row++) {
      for (int col = 0; col < size; col++) {
        cells[row][col] = new Cell(row, col);
        setCreature(row, col, new NullObj());
      }
    }

    createHuman(200);
    createZombie(200);

    executor.submit(new Runnable() {
      int[] indexes = new int[]{rowInMesh, colInMesh};
      Object[] objectsForCells = new Object[2];
      Object[] objectsForZombiesCount = new Object[1];
      Object[] objectsForHumansCount = new Object[1];
      Object[] objectsForExposedCount = new Object[1];
      Object[] objectsForDeadCount = new Object[1];


      @Override
      public void run() {
        while (true) {
          int[][] uiValues = getUIValues();
          try {
            objectsForCells[0] = uiValues;
            objectsForCells[1] = indexes;
            objectsForZombiesCount[0] = getCreatureCount(new Zombie());
            objectsForHumansCount[0] = getCreatureCount(new Human());
            objectsForExposedCount[0] = getCreatureCount(new Exposed());
            objectsForDeadCount[0] = getCreatureCount(new Dead());
            MPI.COMM_WORLD.Send(objectsForCells, 0, 2, MPI.OBJECT, 0, 0);
            MPI.COMM_WORLD.Send(objectsForZombiesCount, 0, 1, MPI.OBJECT, 0, 1);
            MPI.COMM_WORLD.Send(objectsForHumansCount, 0, 1, MPI.OBJECT, 0, 2);
            MPI.COMM_WORLD.Send(objectsForExposedCount, 0, 1, MPI.OBJECT, 0, 9);
            MPI.COMM_WORLD.Send(objectsForDeadCount, 0, 1, MPI.OBJECT, 0, 10);
            moveZombies();
            moveHumans();
            changestateExposed();


          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    });

    executor.submit(new Runnable() {
      Object[] objectsForZombiesCount = new Object[1];

      @Override
      public void run() {
        while (true) {
          Integer zombiesCount = 0;
          try {
            MPI.COMM_WORLD.Recv(objectsForZombiesCount, 0, 1, MPI.OBJECT, 0, 3);
            zombiesCount = (Integer) objectsForZombiesCount[0];
            createZombie(zombiesCount);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    });

    executor.submit(new Runnable() {
      Object[] objectsForHumansCount = new Object[1];

      @Override
      public void run() {
        while (true) {
          Integer humansCount = 0;
          try {
            MPI.COMM_WORLD.Recv(objectsForHumansCount, 0, 1, MPI.OBJECT, 0, 4);
            humansCount = (Integer) objectsForHumansCount[0];
            createHuman(humansCount);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  private void calculateRankForCopyingGhostCells(int rank, int rowSize, int colSize) {
    int row = (rank - 1) / colSize;
    int col = (rank - 1) % colSize;

    southRankToSend = rank + colSize;
    northRankToSend = rank - colSize;
    eastRankToSend = rank + 1;
    westRankToSend = rank - 1;

    if (row == 0) {
      northRankToSend = rank + (colSize) * (rowSize - 1);
    } else if (row == (rowSize - 1)) {
      southRankToSend = rank - (colSize) * (rowSize - 1);
    }

    if (col == 0) {
      westRankToSend = rank + (colSize - 1);
    } else if (col == colSize - 1) {
      eastRankToSend = rank - (colSize - 1);
    }

    northRankToRecv = southRankToSend;
    southRankToRecv = northRankToSend;
    eastRankToRecv = westRankToSend;
    westRankToRecv = eastRankToSend;
  }

  private void setCreature(int i, int j, Creature creature) {
    cells[i][j].setCreature(creature);
  }

  private void createHuman(int count) {
    int numOfHuman = 0;
    while (numOfHuman < count) {
      int x = randomNumberBelow(this.size - 2) + 1;
      int y = randomNumberBelow(this.size - 2) + 1;
      if (isOccupied(x, y) == false) {

        Human human = new Human();
        human.setAge(randomNumberBelow(30));
        setCreature(x, y, human);
        numOfHuman += 1;
      }
    }
  }

  private void createZombie(int count) {
    int numOfZombie = 0;
    while (numOfZombie < count) {
      int x = randomNumberBelow(this.size - 2) + 1;
      int y = randomNumberBelow(this.size - 2) + 1;
      if (isOccupied(x, y) == false) {
        Zombie zombie = new Zombie();
        zombie.setAge(randomNumberBelow(30));
        setCreature(x, y, zombie);
        numOfZombie += 1;
      }
    }
  }

  private int randomNumberBelow(int number) {
    return random.nextInt(number);
  }

  private boolean isOccupied(int i, int j) {
    return this.cells[i][j].isOccupied();
  }

  private void setGhostCellToObject(Object[] objects, int direction) {
    int[] types = new int[this.size];
    int[] age = new int[this.size];
    double[] metabolicRate = new double[this.size];

    //North
    if (direction == 1) {
      for (int i = 0; i < size; i++) {
        types[i] = cells[0][i].getCreature().getTypeAsInt();
        age[i] = cells[0][i].getCreature().getAge();
        metabolicRate[i] = cells[0][i].getCreature().getMetabolicRate();
        cells[0][i].setCreature(new NullObj());
      }
    } else if (direction == 2) { //East
      for (int i = 0; i < size; i++) {
        types[i] = cells[i][this.size - 1].getCreature().getTypeAsInt();
        age[i] = cells[i][this.size - 1].getCreature().getAge();
        metabolicRate[i] = cells[i][this.size - 1].getCreature().getMetabolicRate();
        cells[i][this.size - 1].setCreature(new NullObj());
      }
    } else if (direction == 3) { //South
      for (int i = 0; i < size; i++) {
        types[i] = cells[this.size - 1][i].getCreature().getTypeAsInt();
        age[i] = cells[this.size - 1][i].getCreature().getAge();
        metabolicRate[i] = cells[this.size - 1][i].getCreature().getMetabolicRate();
        cells[this.size - 1][i].setCreature(new NullObj());
      }
    } else if (direction == 4) { // West
      for (int i = 0; i < size; i++) {
        types[i] = cells[i][0].getCreature().getTypeAsInt();
        age[i] = cells[i][0].getCreature().getAge();
        metabolicRate[i] = cells[i][0].getCreature().getMetabolicRate();
        cells[i][0].setCreature(new NullObj());
      }
    }
    objects[0] = types;
    objects[1] = age;
    objects[2] = metabolicRate;
  }

  private void copyObjectsToCells(Object[] objects, int direction) {
    int[] types = new int[this.size];
    int[] age = new int[this.size];
    double[] metabolicRate = new double[this.size];

    types = (int[]) objects[0];
    age = (int[]) objects[1];
    metabolicRate = (double[]) objects[2];
    Creature[] creatures = new Creature[this.size];

    for (int i = 0; i < size; i++) {
      Creature creature = new NullObj();
      if (types[i] == 1) {
        creature = new Human();
      } else if (types[i] == 2) {
        creature = new Zombie();
      }
      creature.setAge(age[i]);
      creature.setMetabolicRate(metabolicRate[i]);
      creatures[i] = creature;
    }
    //North
    if (direction == 3) {
      for (int i = 0; i < size; i++) {
        if (creatures[i].getTypeAsInt() > 0) {
          if(cells[1][i].isOccupied()){
            for (int j = 0; j < size; j++){
              if(cells[1][j].isOccupied() == false){
                cells[1][j].setCreature(creatures[i]);
                break;
              }
            }
          } else{
            cells[1][i].setCreature(creatures[i]);
          }

        }
      }
    } else if (direction == 4) { //East
      for (int i = 0; i < size; i++) {
        if (creatures[i].getTypeAsInt() > 0) {
          if(cells[i][this.size - 2].isOccupied()){
            for (int j = 0; j < size; j++){
              if(cells[j][this.size - 2].isOccupied() == false){
                cells[j][this.size - 2].setCreature(creatures[i]);
                break;
              }
            }
          } else{
            cells[i][this.size - 2].setCreature(creatures[i]);
          }


        }
      }
    } else if (direction == 1) { //South
      for (int i = 0; i < size; i++) {
        if (creatures[i].getTypeAsInt() > 0) {
          if(cells[i][this.size - 2].isOccupied()){
            for (int j = 0; j < size; j++){
              if(cells[this.size - 2][j].isOccupied() == false){
                cells[this.size - 2][j].setCreature(creatures[i]);
                break;
              }
            }
          } else{
            cells[this.size - 2][i].setCreature(creatures[i]);
          }
        }
      }
    } else if (direction == 2) { // West
      for (int i = 0; i < size; i++) {
        if (creatures[i].getTypeAsInt() > 0) {
          if(cells[i][1].isOccupied()){
            for (int j = 0; j < size; j++){
              if(cells[j][1].isOccupied() == false){
                cells[j][1].setCreature(creatures[i]);
                break;
              }
            }
          } else{
            cells[i][1].setCreature(creatures[i]);
          }
        }
      }
    }
  }

  public void moveZombies() {

    for (int row = 1; row < size - 1; row++) {
      for (int col = 1; col < size - 1; col++) {
        statechange++;
        if (cells[row][col].getCreature().getTypeAsInt() == 4) {
          if (statechange % 5 == 0) {
            setCreature(row, col, new NullObj());
          }

        }

        if (cells[row][col].getCreature().getTypeAsInt() == 2) {
          Creature creature = cells[row][col].getCreature();
          //nt no= creature.getTypeAsInt();

          if (creature.getAge() > 100) {
            cells[row][col].setCreature(new NullObj());
            break;
          } else {
//            creature.setAge(creature.getAge()+randomNumberBelow(3));
            creature.setAge(creature.getAge());
          }

          double move = randomNumberBelow(5);

          if (move == 1) { //North
            if (isOccupied(row - 1, col) == false) {
              setCreature(row - 1, col, creature);
              setCreature(row, col, new NullObj());
            } else {
              Creature fcreature = cells[row - 1][col].getCreature();
              int num = fcreature.getTypeAsInt();
              if (num == 1) {

                checkattack(row, col);
                if (attack > 5) {
                  setCreature(row, col, new Dead());
                } else {
                  setCreature(row - 1, col, new Exposed());

                  if (cells[row + 1][col - 1].isOccupied() == false) {
                    setCreature(row + 1, col - 1, creature);
                    setCreature(row, col, new NullObj());
                  } else if (cells[row + 1][col].isOccupied() == false) {
                    setCreature(row + 1, col, creature);
                    setCreature(row, col, creature);
                  } else if (cells[row + 1][col + 1].isOccupied() == false) {
                    setCreature(row + 1, col + 1, creature);
                    setCreature(row, col, creature);
                  } else {
                    setCreature(row, col, new Exposed());
                  }

                }
              }
            }
          } else if (move == 2) { //South
            if (isOccupied(row + 1, col) == false) {
              setCreature(row + 1, col, creature);
              setCreature(row, col, new NullObj());
            } else {
              Creature fcreature = cells[row + 1][col].getCreature();
              int num = fcreature.getTypeAsInt();
              if (num == 1) {
                checkattack(row, col);
                if (attack > 5) {
                  setCreature(row, col, new Dead());
                } else {

                  setCreature(row + 1, col, new Exposed());
                  if (cells[row - 1][col].isOccupied() == false) {
                    setCreature(row - 1, col, creature);
                    setCreature(row, col, new NullObj());
                  } else if (cells[row - 1][col - 1].isOccupied() == false) {
                    setCreature(row - 1, col - 1, creature);
                    setCreature(row, col, creature);
                  } else if (cells[row - 1][col + 1].isOccupied() == false) {
                    setCreature(row - 1, col + 1, creature);
                    setCreature(row, col, creature);
                  } else {
                    setCreature(row, col, new Exposed());
                  }

                }

              }
            }
          } else if (move == 3) { //West
            if (isOccupied(row, col - 1) == false) {
              setCreature(row, col - 1, creature);
              setCreature(row, col, new NullObj());
            } else {
              Creature fcreature = cells[row][col - 1].getCreature();
              int num = fcreature.getTypeAsInt();
              if (num == 1) {
                checkattack(row, col);
                if (attack > 5) {
                  setCreature(row, col, new Dead());
                } else {

                  setCreature(row, col - 1, new Exposed());
                  if (cells[row - 1][col + 1].isOccupied() == false) {
                    setCreature(row - 1, col + 1, creature);
                    setCreature(row, col, new NullObj());
                  } else if (cells[row][col + 1].isOccupied() == false) {
                    setCreature(row, col + 1, creature);
                    setCreature(row, col, creature);
                  } else if (cells[row + 1][col + 1].isOccupied() == false) {
                    setCreature(row + 1, col + 1, creature);
                    setCreature(row, col, creature);
                  } else {
                    setCreature(row, col, new Exposed());
                  }


                }
              }
            }
          } else if (move == 4) { //East
            if (isOccupied(row, col + 1) == false) {
              setCreature(row, col + 1, creature);
              setCreature(row, col, new NullObj());
              col += 1;
            } else {
              Creature fcreature = cells[row][col + 1].getCreature();
              int num = fcreature.getTypeAsInt();
              if (num == 1) {
                checkattack(row, col);
                if (attack > 5) {
                  setCreature(row, col, new Dead());
                } else {
                  setCreature(row, col + 1, new Exposed());
                  if (cells[row - 1][col - 1].isOccupied() == false) {
                    setCreature(row - 1, col - 1, creature);
                    setCreature(row, col, new NullObj());
                  } else if (cells[row][col - 1].isOccupied() == false) {
                    setCreature(row, col - 1, creature);
                    setCreature(row, col, creature);
                  } else if (cells[row + 1][col + 1].isOccupied() == false) {
                    setCreature(row + 1, col + 1, creature);
                    setCreature(row, col, creature);
                  } else {
                    setCreature(row, col, new Exposed());
                  }


                }
              }

            }
          }
        }
      }
    }

    sendGhostCells();
  }


  public void moveHumans() {
    for (int row = 1; row < size - 1; row++) {
      for (int col = 1; col < size - 1; col++) {
        if (cells[row][col].getCreature().getTypeAsInt() == 1) {
          Creature creature1 = cells[row][col].getCreature();
          int nom = creature1.getTypeAsInt();
          if (nom == 1) {
            if (creature1.getAge() > 100) {
              cells[row][col].setCreature(new NullObj());
              break;
            } else {
//	              creature.setAge(creature.getAge()+randomNumberBelow(3));
              creature1.setAge(creature1.getAge());
            }
          }

          double move = randomNumberBelow(5);
          

          if (move == 1) { //North
            if (isOccupied(row - 1, col) == false) {
              setCreature(row - 1, col, creature1);
              setCreature(row, col, new NullObj());
            } else {
            	if(cells[row-1][col].getCreature().getTypeAsInt() == 2){
            		setCreature(row,col,new Exposed());
            		
            	}
            }
          } else if (move == 2) { //South
            if (isOccupied(row + 1, col) == false) {
              setCreature(row + 1, col, creature1);
              setCreature(row, col, new NullObj());
            }
            else {
            	if(cells[row+1][col].getCreature().getTypeAsInt() == 2){
            		setCreature(row+1,col,new Exposed());
            		
            	}
            }
          } else if (move == 3) { //West
            if (isOccupied(row, col - 1) == false) {
              setCreature(row, col - 1, creature1);
              setCreature(row, col, new NullObj());
            }
            else {
            	if(cells[row][col-1].getCreature().getTypeAsInt() == 2){
            		setCreature(row,col-1,new Exposed());
            		
            	}
            }

          } else if (move == 4) { //East
            if (isOccupied(row, col + 1) == false) {
              setCreature(row, col + 1, creature1);
              setCreature(row, col, new NullObj());
              col += 1;
            }
            else {
            	if(cells[row][col+1].getCreature().getTypeAsInt() == 2){
            		setCreature(row,col+1,new Exposed());
            		
            	}
            }
          }
        }
      }
    }

    sendGhostCells();
  }


  private void sendGhostCells() {
    try {
      Object[] ghostCellsForNorthToSend = new Object[3];
      Object[] ghostCellsForEastToSend = new Object[3];
      Object[] ghostCellsForSouthToSend = new Object[3];
      Object[] ghostCellsForWestToSend = new Object[3];

      Object[] ghostCellsForNorthToRecv = new Object[3];
      Object[] ghostCellsForEastToRecv = new Object[3];
      Object[] ghostCellsForSouthToRecv = new Object[3];
      Object[] ghostCellsForWestToRecv = new Object[3];

      setGhostCellToObject(ghostCellsForNorthToSend, 1);
      setGhostCellToObject(ghostCellsForEastToSend, 2);
      setGhostCellToObject(ghostCellsForSouthToSend, 3);
      setGhostCellToObject(ghostCellsForWestToSend, 4);

      MPI.COMM_WORLD.Isend(ghostCellsForNorthToSend, 0, 3, MPI.OBJECT, northRankToSend, 5);
      MPI.COMM_WORLD.Recv(ghostCellsForNorthToRecv, 0, 3, MPI.OBJECT, northRankToRecv, 5);
      copyObjectsToCells(ghostCellsForNorthToRecv, 1);

      MPI.COMM_WORLD.Isend(ghostCellsForEastToSend, 0, 3, MPI.OBJECT, eastRankToSend, 6);
      MPI.COMM_WORLD.Recv(ghostCellsForEastToRecv, 0, 3, MPI.OBJECT, eastRankToRecv, 6);
      copyObjectsToCells(ghostCellsForEastToRecv, 2);

      MPI.COMM_WORLD.Isend(ghostCellsForSouthToSend, 0, 3, MPI.OBJECT, southRankToSend, 7);
      MPI.COMM_WORLD.Recv(ghostCellsForSouthToRecv, 0, 3, MPI.OBJECT, southRankToRecv, 7);
      copyObjectsToCells(ghostCellsForSouthToRecv, 3);

      MPI.COMM_WORLD.Isend(ghostCellsForWestToSend, 0, 3, MPI.OBJECT, westRankToSend, 8);
      MPI.COMM_WORLD.Recv(ghostCellsForWestToRecv, 0, 3, MPI.OBJECT, westRankToRecv, 8);
      copyObjectsToCells(ghostCellsForWestToRecv, 4);


    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public int getCreatureCount(Creature creature) {
    int creatureCount = 0;
    for (int row = 1; row < size - 1; row++) {
      for (int col = 1; col < size - 1; col++) {
        if (cells[row][col].getCreature().getClass() == creature.getClass()) {
          creatureCount += 1;
        }
      }
    }
    return creatureCount;
  }

  public int[][] getUIValues() {
    int[][] uiValues = new int[this.size - 2][this.size - 2];
    for (int row = 1; row < size - 1; row++) {
      for (int col = 1; col < size - 1; col++) {
        uiValues[row - 1][col - 1] = cells[row][col].getUIValue();
      }
    }
    return uiValues;
  }


  public int checkattack(int row, int col) {
    int s[] = {0, 1, -1};

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        int num1 = cells[row + s[i]][col + s[j]].getCreature().getTypeAsInt();
        if (num1 == 1) {
          attack++;
        }
      }
    }
    return attack;

  }

  public void changestateExposed() {
    for (int row = 1; row < size - 1; row++) {
    	statechange++;
      for (int col = 1; col < size - 1; col++) {
        
        if (cells[row][col].getCreature().getTypeAsInt() == 3) {
          if (statechange % 100 == 0) {
            setCreature(row, col, new Zombie());
          }


        }
      }

    }
  }


}

//public void zombieattack(int xpos, int ypos, Creature creature) {
//if (cells[xpos][ypos].getCreature().getTypeAsInt() == 2){
//for()

//}
//}

//}

