package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;

public class Zombie extends Creature {
  @Override
  boolean isOccupied() {
    return true;
  }

  @Override
  public void display() {
    System.out.print("Z");
  }

  @Override
  public int getUICreature() {
    return UICreature.Zombie.intValue();
  }

  @Override
  public int getTypeAsInt() {
    return 2;
  }
}
