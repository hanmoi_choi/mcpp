package unimelb.mcpp.models;

public class Message {
  private String command;


  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }
}
