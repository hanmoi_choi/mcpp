package unimelb.mcpp.models;

import unimelb.mcpp.gui.UICreature;

public abstract class Creature {
  protected int age;
  protected double metabolicRate;

  abstract boolean isOccupied();
  public abstract void display();
  public abstract int getUICreature();

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public double getMetabolicRate() {
    return metabolicRate;
  }

  public void setMetabolicRate(double metabolicRate) {
    this.metabolicRate = metabolicRate;
  }

  public abstract int getTypeAsInt();

  @Override
  public String toString() {
    return "Creature{" +
           "age=" + age +
           ", metabolicRate=" + metabolicRate +
           '}';
  }
}
