package unimelb.mcpp.models;
import unimelb.mcpp.gui.UICreature;
public class Exposed extends Creature {
	
	@Override
	  boolean isOccupied() {
	    return true;
	  }

	  @Override
	  public void display() {
	    System.out.print("E");
	  }

	  @Override
	  public int getUICreature() {
	    return UICreature.Exposed.intValue();
	  }

	  @Override
	  public int getTypeAsInt() {
	    return 3;
	  }
     
	 // time it changes from exposed to zombie
	  public int seconds() {
		  int s =0;
		  return s;
	  }

}
