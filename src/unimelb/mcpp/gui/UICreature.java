package unimelb.mcpp.gui;

public enum UICreature {
  Empty(0),  Human(1), Zombie(2),Exposed(3),Dead(4);
  private final int value;

  UICreature(int value) {
    this.value = value;
  }

  public int intValue(){
    return value;
  }
}
