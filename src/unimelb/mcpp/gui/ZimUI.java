package unimelb.mcpp.gui;

import java.awt.*;

import javax.swing.*;

import static unimelb.mcpp.gui.UICreature.Empty;

public class ZimUI extends JFrame {

  private final int PIXEL_SIZE = 2;
  private final int rowSize;
  private final int windowWidth;
  private final int meshCellSize;
  private final int colSize;
  private int[][] uiCreatures;
  private final CanvasPanel canvasPanel;
  private final int windowHeight;
  private final JLabel zombieCount;
  private final JLabel humanCount;
  private final JLabel exposedCount;
  private final JLabel deadCount;

  public ZimUI(int rowSize, int colSize, int meshCellSize) {
    this.rowSize = rowSize;
    this.colSize = colSize;
    windowHeight = rowSize * PIXEL_SIZE * meshCellSize;
    windowWidth = colSize * PIXEL_SIZE * meshCellSize;
    this.meshCellSize = meshCellSize;
    uiCreatures = new int[rowSize * meshCellSize][colSize * meshCellSize];
    for (int row = 0; row < rowSize; row++) {
      for (int col = 0; col < colSize; col++) {
        uiCreatures[row][col] = Empty.intValue();
      }
    }
    this.setSize(new Dimension(windowWidth, windowHeight + 50));
    canvasPanel = new CanvasPanel();
    canvasPanel.setSize(new Dimension(windowWidth, windowHeight));

    JPanel populationLabel = new JPanel();
    populationLabel.setSize(new Dimension(windowWidth, 50));
    JLabel zombieTitle = new JLabel("Zombie");
    zombieTitle.setForeground(Color.RED);
    zombieCount = new JLabel("0");
    JLabel humanTitle = new JLabel("Human");
    humanTitle.setForeground(Color.BLUE);
    humanCount = new JLabel("0");
    JLabel exposedTitle = new JLabel("Exposed");
    exposedTitle.setForeground(Color.ORANGE);
    exposedCount = new JLabel("0");
    JLabel deadTitle = new JLabel("Dead");
    deadTitle.setForeground(Color.green);
    deadCount = new JLabel("0");
    populationLabel.add(zombieTitle);
    populationLabel.add(zombieCount);
    populationLabel.add(humanTitle);
    populationLabel.add(humanCount);
    populationLabel.add(exposedTitle);
    populationLabel.add(exposedCount);
    populationLabel.add(deadTitle);
    populationLabel.add(deadCount);

    this.getContentPane().setLayout(new BorderLayout());
    this.getContentPane().add(populationLabel, BorderLayout.NORTH);
    this.getContentPane().add(canvasPanel, BorderLayout.CENTER);
    this.setLocation(200, 200);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);
  }

  public void updateUICreatures(int startX, int startY, int[][] cells) {
    for (int row = 0; row < cells.length; row++) {
      for (int col = 0; col < cells.length; col++) {
        uiCreatures[row + (meshCellSize * startX)][col + (meshCellSize * startY)] = cells[row][col];
      }
    }
  }

  public void updateUI() {
    canvasPanel.repaint();
  }

  public void updateZombiesCount(Integer zombileSum) {
    zombieCount.setText(String.valueOf(zombileSum));
  }

  public void updateHumansCount(Integer humanSum) {
    humanCount.setText(String.valueOf(humanSum));
  }

  public void updateExposedCount(Integer ExposedSum) {
    exposedCount.setText(String.valueOf(ExposedSum));
  }

  public void updateDeadCount(Integer deadSum) {
    deadCount.setText(String.valueOf(deadSum));
  }

  class CanvasPanel extends JPanel {

    private int x1, y1, x2, y2;
    private int lastPixel;

    CanvasPanel() {
      lastPixel = windowHeight + 1;
    }

    public void paintComponent(Graphics g) {
      for (int row = 0; row < rowSize * meshCellSize; row++) {
        for (int col = 0; col < colSize * meshCellSize; col++) {
          switch (uiCreatures[row][col]) {
            case 0:
              g.setColor(Color.WHITE);
              break;
            case 1:
              g.setColor(Color.BLUE);
              break;
            case 2:
              g.setColor(Color.RED);
              break;
            case 3:
              g.setColor(Color.ORANGE);

              break;
            case 4:
              g.setColor(Color.GREEN);
              break;
          }
          g.fillRect((col) * PIXEL_SIZE, (row) * PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE);
        }
      }
      g.setColor(Color.black);
    }
  }
}
