package unimelb.mcpp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import mpi.MPI;
import unimelb.mcpp.gui.ZimUI;
import unimelb.mcpp.models.CellMesh;

public class ZmiMain {

  private static final int NUM_OF_RECV_THREADS = 5;

  public static void main(String[] args) {
    /*
     * Tag 0 is for updating UI
     * Tag 1 is to calculate total number of Zombies
     * Tag 2 is to calculate total number of Humans
     * Tag 3 is to control the number of zombies
     * Tag 4 is to control the number of humans
     * Tag 5 is to copy ghost cell to North Mesh
     * Tag 6 is to copy ghost cell to East  Mesh
     * Tag 7 is to copy ghost cell to South Mesh
     * Tag 8 is to copy ghost cell to West Mesh
     * Tag 9 is to calculate total number of Exposed
     * Tag 10 is to calculate total number of Dead Body
     */
    final int numOfProcesses = Integer.valueOf(args[1]);
    int meshRowSize = (int) (numOfProcesses-1) / 4;
    int meshColSize = (int) (numOfProcesses-1) / 3;
    int meshCellSize = 100;

    MPI.Init(args);
    int size = MPI.COMM_WORLD.Size();
    int rank = MPI.COMM_WORLD.Rank();
    int master = 0;

    if(rank == master){
      ExecutorService executor = Executors.newFixedThreadPool(NUM_OF_RECV_THREADS);
      final ZimUI zimUI = new ZimUI(meshRowSize, meshColSize, meshCellSize);

      //Upate UI
      executor.submit(new Runnable() {
        @Override
        public void run() {
          while (true){
            try{
              Object[] objects = new Object[2];
              int[][] uiValues = new int[100][100];
              int[] indexes = new int[2];
              for(int i = 1 ; i < numOfProcesses; i++){
                MPI.COMM_WORLD.Recv(objects, 0, 2, MPI.OBJECT, i, 0);
                uiValues = (int[][]) objects[0];
                indexes = (int[])objects[1];
                zimUI.updateUICreatures(indexes[0], indexes[1], uiValues);
              }
              zimUI.updateUI();
            } catch (Exception e){
              e.printStackTrace();
            }

//            try {
//              Thread.sleep(10);
//            } catch (InterruptedException e) {
//              e.printStackTrace();
//            }
          }
        }
      });
      //Update the number of zombies in UI and manage it
      executor.submit(new Runnable() {
        @Override
        public void run() {
          while (true){
            Integer zombileSum = 0;
            Object[] objectsForZombiesCount = new Object[1];
            for(int i = 1 ; i < numOfProcesses; i++){
              MPI.COMM_WORLD.Recv(objectsForZombiesCount, 0, 1, MPI.OBJECT, i, 1);
              zombileSum += (Integer)objectsForZombiesCount[0];
            }

            zimUI.updateZombiesCount(zombileSum);

            if(zombileSum < 500){
              Object[] newZombies = new Object[]{50};
              for(int i = 1 ; i < numOfProcesses; i++){
                MPI.COMM_WORLD.Send(newZombies, 0, 1, MPI.OBJECT, i, 3);
              }
            }
            try {
              Thread.sleep(100);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      });

      //Update the number of humans in UI and manage it
      executor.submit(new Runnable() {
        @Override
        public void run() {
          while (true){
            Integer humansSum = 0;
            Object[] objectsForHumansCount = new Object[1];

            for(int i = 1 ; i < numOfProcesses; i++){
              MPI.COMM_WORLD.Recv(objectsForHumansCount, 0, 1, MPI.OBJECT, i, 2);
              humansSum += (Integer)objectsForHumansCount[0];
            }

            zimUI.updateHumansCount(humansSum);

            if(humansSum < 500){
              Object[] newHumans = new Object[]{50};
              for(int i = 1 ; i < numOfProcesses; i++){
                MPI.COMM_WORLD.Send(newHumans, 0, 1, MPI.OBJECT, i, 4);
              }
            }

//            try {
//              Thread.sleep(100);
//            } catch (InterruptedException e) {
//              e.printStackTrace();
//            }
          }
        }
      });
      //Update the number of Exposed in UI and manage it
      executor.submit(new Runnable() {
        @Override
        public void run() {
          while (true){
            Integer exposedSum = 0;
            Object[] objectsForZombiesCount = new Object[1];
            for(int i = 1 ; i < numOfProcesses; i++){
              MPI.COMM_WORLD.Recv(objectsForZombiesCount, 0, 1, MPI.OBJECT, i, 9);
              exposedSum += (Integer)objectsForZombiesCount[0];
            }

            zimUI.updateExposedCount(exposedSum);
          }
        }
      });
      //Update the number of Dead in UI and manage it
      executor.submit(new Runnable() {
        @Override
        public void run() {
          while (true){
            Integer deadSum = 0;
            Object[] objectsForZombiesCount = new Object[1];
            for(int i = 1 ; i < numOfProcesses; i++){
              MPI.COMM_WORLD.Recv(objectsForZombiesCount, 0, 1, MPI.OBJECT, i, 10);
              deadSum += (Integer)objectsForZombiesCount[0];
            }

            zimUI.updateDeadCount(deadSum);

          }
        }
      });
    }else{
//      System.out.println("S: " +sizeMesh);
      int row = (rank-1) / meshColSize;
      int col = (rank-1) % meshColSize;
      new CellMesh(row, col, meshCellSize+2, rank, meshRowSize, meshColSize);

    }
    MPI.Finalize();

  }
}
